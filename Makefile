# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lde-batz <lde-batz@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/22 11:55:15 by lde-batz          #+#    #+#              #
#    Updated: 2019/01/21 14:03:44 by jpelleti         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_ls

SRC = 	ft_opt.c		\

SRC_DIR = srcs/

OBJ_DIR = srcs/

OBJ := $(addprefix $(OBJ_DIR), $(SRC:.c=.o))

SRC := $(addprefix $(SRC_DIR), $(SRC))

INC =	libft/libft.h -I libft/libft.h

LIB = libft/ -lft

GCC = gcc -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OBJ)
	make -C libft
	$(GCC) -o $(NAME) $(OBJ) -L $(LIB)

$(OBJ_DIR)%.o: $(SRC_DIR)%.c
	$(GCC) -c -o $@ $< -I $(INC)

clean:
	rm -f $(OBJ)
	make -C libft clean

fclean: clean
	rm -rf $(NAME)
	make -C libft fclean

re: fclean all

.PHONY: all clean fclean re
