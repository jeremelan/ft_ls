/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_test.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpelleti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 10:41:33 by jpelleti          #+#    #+#             */
/*   Updated: 2019/01/21 14:03:00 by jpelleti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"
typedef struct dirent t_dirent;

t_list	*ft_dcrs_sort_list(t_list *first, char *name)
{
	t_list		*nav[3];

	nav[CURR] = first;
	if (!(nav[NEW] = malloc(sizeof(t_list))))
		return (NULL);
	nav[NEW]->content = name;
	if (ft_strcmp(nav[NEW]->content, nav[CURR]->content) >= 0)
	{
		nav[NEW]->next = first;
		return (nav[NEW]);
	}
	while ((nav[PREV] = nav[CURR]) && (nav[CURR] = nav[CURR]->next))
	{
		if (ft_strcmp(nav[NEW]->content, nav[CURR]->content) >= 0)
		{
			nav[NEW]->next = nav[CURR];
			nav[PREV]->next = nav[NEW];
			return (first);
		}
	}
	nav[PREV]->next = nav[NEW];
	nav[NEW]->next = NULL;
	return (first);
}

t_list	*ft_icrs_sort_list(t_list *first, char *name)
{
	t_list		*nav[3];

	nav[CURR] = first;
	if (!(nav[NEW] = malloc(sizeof(t_list))))
		return (NULL);
	nav[NEW]->content = name;
	if (ft_strcmp(nav[NEW]->content, nav[CURR]->content) <= 0)
	{
		nav[NEW]->next = first;
		return (nav[NEW]);
	}
	while ((nav[PREV] = nav[CURR]) && (nav[CURR] = nav[CURR]->next))
	{
		if (ft_strcmp(nav[NEW]->content, nav[CURR]->content) <= 0)
		{
			nav[NEW]->next = nav[CURR];
			nav[PREV]->next = nav[NEW];
			return (first);
		}
	}
	nav[PREV]->next = nav[NEW];
	nav[NEW]->next = NULL;
	return (first);
}


int	main(int ac, char **av)
{
	DIR			*dir;
	t_dirent	*lect;
	t_list		*lst;

	if (!(lst = malloc(sizeof(t_list))))
		return (1);
	(void)ac;
	if (!(dir = opendir(av[1])))
	{
		perror("");
		exit (EXIT_FAILURE);
	}
	lect = readdir(dir);
	lst->content = lect->d_name;
	lst->next = NULL;
	while ((lect = readdir(dir)))
		lst = ft_icrs_sort_list(lst, lect->d_name);
	if (errno != 0)
	{
		perror("");
		exit (EXIT_FAILURE);
	}
	if (closedir(dir) == -1)
	{
		perror("");
		exit (EXIT_FAILURE);
	}
	while(lst)
	{
		printf("%s \n", lst->content);
		lst = lst->next;
	}
	return (0);
}
