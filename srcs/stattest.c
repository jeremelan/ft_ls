/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stattest.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpelleti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/20 15:17:31 by jpelleti          #+#    #+#             */
/*   Updated: 2019/01/15 12:12:43 by jpelleti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"
#include <time.h>
typedef struct stat t_stat;
typedef struct passwd t_passwd;
typedef struct group t_group;

char	*ft_type(mode_t mod)
{
	if (S_ISREG(mod))
		return("Fichier");
	if (S_ISDIR(mod))
		return("Repertoire");
	if (S_ISBLK(mod))
		return("Bloc");
	if (S_ISLNK(mod))
		return("lien");
	return ("");
}

char	*ft_modes(mode_t mod)
{
	char		*s;
	unsigned	x;

	x = 0x0001;
	s = ft_strnew(9);
	s[0] = (x << 8) & mod ? 'r' : '-';
	s[1] = (x << 7) & mod ? 'w' : '-';
	s[2] = (x << 6) & mod ? 'x' : '-';
	s[3] = (x << 5) & mod ? 'r' : '-';
	s[4] = (x << 4) & mod ? 'w' : '-';
	s[5] = (x << 3) & mod ? 'x' : '-';
	s[6] = (x << 2) & mod ? 'r' : '-';
	s[7] = (x << 1) & mod ? 'w' : '-';
	s[8] = (x << 0) & mod ? 'x' : '-';
	return (s);
}

int	main(int ac, char **av)
{
	t_stat		*stt;
	t_passwd	*pwd;
	t_group		*grp;
	char		*stock;
	char		*del;

	(void)ac;
	if(!(stt = malloc(sizeof(t_stat))))
		exit (EXIT_FAILURE);
	if (lstat((av[1]), stt) == -1)
	{
		perror("");
		exit(EXIT_SUCCESS);
	}
	if (!(pwd = getpwuid(stt->st_uid)))
	{
		perror("");
		exit(EXIT_SUCCESS);
	}
	if (!(grp = getgrgid(stt->st_gid)))
	{
		perror("");
		exit(EXIT_SUCCESS);
	}
	stock = ft_strjoin("", ctime(&stt->st_mtime));
	del = ft_strrchr(stock, ' ');
	*(del - 3) = '\0';
	ft_putendl(ft_strjoin("Nom: ", av[1]));
	ft_putendl(ft_strjoin("Type: ", ft_type(stt->st_mode)));
	ft_putendl(ft_strjoin("Modes: ", ft_modes(stt->st_mode)));
	ft_putstr("Nombre de liens: ");
	ft_putnbr(stt->st_nlink);
	ft_putendl(ft_strjoin("\nProprietaire: ", pwd->pw_name));
	ft_putendl(ft_strjoin("Groupe: ", grp->gr_name));
	ft_putstr("Taille: ");
	ft_putnbr(stt->st_size);
	ft_putendl(" octets");
	ft_putendl(ft_strjoin("Date de derniere modification: ", stock + 4));
	return (0);
}
