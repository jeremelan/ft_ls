/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_opt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpelleti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/21 13:17:46 by jpelleti          #+#    #+#             */
/*   Updated: 2019/01/21 14:19:24 by jpelleti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"

void	error_opt(char err)
{
	ft_putstr("ls: illegal option -- ");
	ft_putchar(err);
	ft_putchar('\n');
	ft_putendl("usage: ./ft_ls [-Ralrt] [file ...]");
}

int		*ft_opt(int *i, char **av)
{
	int j;
	int *tab;

	*i = 1;
	if (!(tab = malloc(sizeof(int) * 5)))
		return (0);
	while(av[*i])
	{
		j = 0;
		while (av[*i][j])
		{
			if (j >= 1 && av[*i][j] != 'l' && av[*i][j] != 'a' &&
				av[*i][j] != 'R' && av[*i][j] != 'r' && av[*i][j] != 't')
			{
				if (j == 1 && ft_strlen(av[*i]) == 2 && av[*i][j] == '-')
					return (tab);
				error_opt(av[*i][j]);
				return (0);
			}
			j != 0 && av[*i][j] == 'l' ? tab[LNG] = 1 : 0 ;
			j != 0 && av[*i][j] == 'a' ? tab[ALL] = 1 : 0 ;
			j != 0 && av[*i][j] == 'R' ? tab[REC] = 1 : 0 ;
			j != 0 && av[*i][j] == 'r' ? tab[REV] = 1 : 0 ;
			j != 0 && av[*i][j] == 't' ? tab[LST] = 1 : 0 ;
			j++;
		}
		(*i)++;
	}
	return (tab);
}

int		main(int ac, char **av)
{
	int *tab;
	int cpt;

	(void)ac;
	tab = ft_opt(&cpt, av);
	printf("LNG -> %d\n", tab[LNG]);
	printf("ALL -> %d\n", tab[ALL]);
	printf("REC -> %d\n", tab[REC]);
	printf("REV -> %d\n", tab[REV]);
	printf("LST -> %d\n", tab[LST]);
	printf("Compteur de mots : %d\n", cpt);
	return (0);
}
